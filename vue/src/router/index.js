import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import MainPage from '../views/MainPage.vue'
import LhjView from '../views/LhjView.vue'
import zjView from '../views/zjView.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/home',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/',
      name: 'main',
      component: MainPage
    },
    {
      path: '/LhjView',
      name: 'Lhj',
      component: LhjView
    },
    {
      path: '/fry',
      name: 'fry',
      component: () => import('../views/fryView.vue')
    },
    {
      path: '/yjp',
      name: 'yjp',
      component: () => import('../views/yjpView.vue')
    },
    {
      path: '/pjp',
      name: 'pjp',
      component: () => import('../views/PjpView.vue')
    },
    {
      path:'/dsj',
      name:'dsj',
      component:() => import('../views/dsjview.vue')
    },
    {
      path:'/dtt',
      name:'dtt',
      component:() => import('../views/dttview.vue')
    },
    {
      path:'/hm',
      name:'hm',
      component:() => import('../views/hmview.vue')
    },
    {
      path:'/zj',
      name:'zj',
      component:() => import('../views/zjview.vue')
    },
    {
      path:'/hjw',
      name:'hjw',
      component:() => import('../views/hjwView.vue')
    },
    {
      path:'/lmh',
      name:'lmh',
      component:() => import('../views/lmhView.vue')
    }
  ]
})

export default router
