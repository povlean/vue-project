import {defineStore} from 'pinia';


export const lmhCountStore = defineStore("lmhcount",{
    state:()=>({
        name: "李梦晗",
        age: 20
    }),
    actions: {
        addage(){
            this.age++
        },
        lessage(){
            this.age--
        },
    },
})

