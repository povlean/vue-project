import { defineStore } from 'pinia'

export const PjpCountStore = defineStore({
  id: 'main',
  state: () => ({
    name:"蒲金鹏",
    age: 21,
    local:"资阳",
  }),
  actions: {
    decreaseAge() {
      this.age--
    },
    increaseAge() {
      this.age++
    },
  },
})