import { defineStore } from 'pinia'

export const hjwStore = defineStore('hjw',{
    state: () => {
        return {
            name:'韩家伟',
            age:19,
        }
    },
    getters:{},
    actions: {
        addage(){
            this.age++;
        },
        subage(){
            this.age--;
        }
    }
})