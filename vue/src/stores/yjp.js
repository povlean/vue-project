import {defineStore} from 'pinia';
export const yjpStore = defineStore('yjp', {
    state:()=>{
        return{
            username:'杨金平',
            phone:'13984214263',
            age:21,
            position:'贵州省',
            school:'西财计科05班',
            contentPosition:'贵州省遵义市习水县'
        }
    },
    getters:{},
    actions:{
        ageAdd(){
            this.age++;
        },
        restore(){
            this.age = 21;
        }
    }
    
})