import {defineStore} from 'pinia';
export const dttStore = defineStore('dtt', {
    state:()=>{
        return{
            username:'杜婷婷',
            phone:'13989259324',
            age:19,
            position:'四川省',
            school:'西财天府学院',
            contentPosition:'绵阳'
        }
    },
    actions:{
        ageAdd(){
            this.age++;
        },
        restore(){
            this.age = 19;
        },
        ageDown(){
            this.age--;
        }
    }
    
})