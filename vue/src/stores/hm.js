import { defineStore } from 'pinia';
export const hmStore = defineStore('hm', {
    state: () => {
        return {
            name: '黄敏',
            age: '21',
            address: '四川省眉山市',
            phone: '18881628315',
        }
    },
    getters: {},
    actions: {
        ageAdd() {
            this.age++;
        },
        ageAdd2() {
            this.age--;
        },
        return() {
            this.age = 21;
        }
    }

})