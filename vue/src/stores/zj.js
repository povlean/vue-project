import {defineStore} from 'pinia';
export const zjStore = defineStore('zj', {
    state:()=>{
        return{
            username:'张静',
            phone:'18881652582',
            age:19,
            position:'四川达州',
            contentPosition:'西南财经大学天府学院',
            grade:'2021级智科02班',
            shownumber:0
        }
    },
    getters:{},
    actions:{
        numberA(){
            this.shownumber++;
        },
        numberD(){
            this.shownumber--;
        }
    }
    
})