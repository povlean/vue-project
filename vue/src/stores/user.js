import { defineStore } from 'pinia'
export const useUsersStore = defineStore('users', {
  state: () => (
    { 
      counter: 0 , 
      name: 'Povlean', 
      age: 25, 
      gender: '男'
    }
  ),
  getters: {
    doubleCount: (state) => state.counter * 2,
    // 自动推导返回类型
    doubleCount(state) {
      return state.counter * 2
    },
    // 依赖getters返回参数，则需要显性的设置返回类型
    doublePlusOne() {
      return this.doubleCount + 1
    },
  },
  actions: {
    increment() {
      this.counter++
    },
    randomizeCounter() {
      this.counter = Math.round(100 * Math.random())
    },
  },
})
