import { defineStore } from 'pinia';


export const dsjstore = defineStore("dsjcount", {

    state: () => ({

        name: "邓少杰",
        age: 20,
        sex: "男"
    }),
    actions: {
        agejia() {
            this.age++
        },
        agejian() {
            this.age--
        },
        sexchange() {
            if (this.sex == "男") {
                this.sex = "女";
            }
            else {
                this.sex = "男";
            }

        }
    },
})
